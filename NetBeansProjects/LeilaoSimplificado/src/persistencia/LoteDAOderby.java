/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import negocio.Lote;
import negocio.LoteDAO;
import static persistencia.DAOderby.getConnection;

/**
 *
 * @author lucasfeijo
 */
public class LoteDAOderby extends DAOderby implements LoteDAO {
    
    private static LoteDAOderby ref;
    
    private LoteDAOderby() throws Exception {
        super();
    }
    
    public static LoteDAOderby getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new LoteDAOderby();
            } return ref;
        } catch (Exception ex) {
            throw new DAOException("Erro no DAO, "+ex.toString(), ex);
        }
    }

    @Override
    public Lote buscar(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LOTE WHERE id_leilao=?");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Lote p = new Lote(resultado.getString("CATEGORIA"),
                                  resultado.getString("DESCRICAO"),
                                  resultado.getInt("ID_LEILAO"));
                return p;
            } else {
                throw new DAOException("Falha no LoteDAOderby (busca): nenhum item retornado");
            }
        } catch (SQLException ex) {
            throw new DAOException("Falha no LoteDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no LoteDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public boolean inserir(Lote l) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO APP.LOTE (id_leilao, descricao, categoria) VALUES (?,?,?)");
            stmt.setInt(1, l.getIdLeilao());
            stmt.setString(2, l.getDescricao());
            stmt.setString(3, l.getCategoria());
            stmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Falha no LoteDAOderby (insercao), "+ex.toString(), ex);
        } catch (Exception ex) {
            throw new DAOException("Falha no LoteDAOderby (conexao), "+ex.toString(), ex);
        }
    }
    
}