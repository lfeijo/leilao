/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import negocio.Leilao;
import negocio.LeilaoDAO;
import negocio.TipoLeilao;
import static persistencia.DAOderby.getConnection;

/**
 *
 * @author lucasfeijo
 */
public class LeilaoDAOderby extends DAOderby implements LeilaoDAO {
    
    private static LeilaoDAOderby ref;
    
    private LeilaoDAOderby() throws Exception{
        super();
    }
    
    public static LeilaoDAOderby getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new LeilaoDAOderby();
            } return ref;
        } catch (Exception ex) {
            throw new DAOException("Erro no LeilaoDAOderby, "+ex.toString(), ex);
        }
    }
    
    @Override
    public int inserir(Leilao l) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO APP.LEILAO (INICIO, FIM, FECHADO, TIPO_LEILAO, PRECO_CONDICAO, ID_USER) VALUES (?,?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setLong(1, l.getInicio());
            stmt.setLong(2, l.getFim());
            stmt.setBoolean(3, l.isFechado());
            stmt.setInt(4, l.getTipoAsInt());
            stmt.setDouble(5, l.getPrecoCondicao());
            stmt.setInt(6, l.getId_user());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            int id;
            if(rs.next()) { id = rs.getInt(1); con.close(); return id; }
            
        } catch (SQLException ex) {
            throw new DAOException("Falha no LeilaoDAOderby (insercao), "+ex.toString(), ex);
        } catch (Exception ex) {
            throw new DAOException("Falha no LeilaoDAOderby (conexao), "+ex.toString(), ex);
        }
        return 0;
    }

    @Override
    public List<Leilao> buscar() throws DAOException {
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet resultado = stmt.executeQuery("SELECT * FROM APP.LEILAO");
            List<Leilao> lista = new ArrayList<>();
            while(resultado.next()) {
                Leilao l = new Leilao(resultado.getInt("id_leilao"),
                                      resultado.getLong("inicio"),
                                      resultado.getLong("fim"),
                                      resultado.getDouble("preco_condicao"),
                                      resultado.getBoolean("fechado"),
                                      TipoLeilao.values()[resultado.getInt("tipo_leilao")],
                                      resultado.getInt("id_user"));
                lista.add(l);
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOException("Falha no LeilaoDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no LeilaoDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public Leilao buscar(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LEILAO WHERE id_leilao=?");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Leilao l = new Leilao(resultado.getInt("id_leilao"),
                                    resultado.getLong("inicio"),
                                    resultado.getLong("fim"),
                                    resultado.getDouble("preco_condicao"),
                                    resultado.getBoolean("fechado"),
                                    TipoLeilao.values()[resultado.getInt("tipo_leilao")],
                                    resultado.getInt("id_user"));
                return l;
            } else {
                throw new DAOException("Falha no LeilaoDAOderby (busca): nenhum resultado");
            }

        } catch (SQLException ex) {
            throw new DAOException("Falha no LeilaoDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no LeilaoDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }
    
}
