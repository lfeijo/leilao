package persistencia;

import java.util.List;
import negocio.UserDAO;
import negocio.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author lucasfeijo
 */ 
public class UserDAOderby extends DAOderby implements UserDAO {

    private static UserDAOderby ref;
    
    private UserDAOderby() throws DAOException, Exception {
        super();
    }
    
    public static UserDAOderby getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new UserDAOderby();
            } return ref;
        } catch (Exception ex) {
            throw new DAOException("Erro no UserDAOderby, "+ex.toString(), ex);
        }
    }
    
    @Override
    public int inserir(User u) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO APP.USER_ (NOME, ID_GOV, EMAIL) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, u.getNome());
            stmt.setString(2, u.getIdGov());
            stmt.setString(3, u.getEmail());
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            int id;
            if(rs.next()) {
                id = rs.getInt(1);
                con.close();
                return id;
            }
        } catch (SQLException ex) {
            throw new DAOException("Falha no UserDAOderby (insercao), "+ex.toString(), ex);
        } catch (Exception ex) {
            throw new DAOException("Falha no UserDAOderby (conexao), "+ex.toString(), ex);
        }
        return 0;
    }

    @Override
    public List<User> buscar() throws DAOException {
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet resultado = stmt.executeQuery("SELECT * FROM APP.USER_");
            List<User> lista = new ArrayList<>();
            while(resultado.next()) {
                int id_user = resultado.getInt("ID_USER");
                String nome = resultado.getString("NOME");
                String id_gov = resultado.getString("ID_GOV");
                String email = resultado.getString("EMAIL");
                User p = new User(id_user, nome, id_gov, email);
                lista.add(p);
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOException("Falha no UserDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no UserDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public User buscar(int id_user) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.USER_ WHERE id_user=?");
            stmt.setInt(1, id_user);
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                User l = new User(resultado.getInt("id_user"),
                                    resultado.getString("nome"),
                                    resultado.getString("id_gov"),
                                    resultado.getString("email"));
                return l;
            } else {
                throw new DAOException("Falha no LeilaoDAOderby (busca): nenhum resultado");
            }

        } catch (SQLException ex) {
            throw new DAOException("Falha no LeilaoDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no LeilaoDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

}
