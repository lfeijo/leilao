/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author lucasfeijo
 */
public class DAOderby {

    protected DAOderby() throws DAOException, Exception {
        try {
             Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        } catch (ClassNotFoundException ex) {
            throw new DAOException("JdbcOdbDriver not found");
        }
    }
    
    protected static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:derby://localhost:1527/leilao2;create=true;user=app;password=password");
    }
    
}
