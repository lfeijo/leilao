/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import negocio.Bem;
import negocio.BemDAO;
import static persistencia.DAOderby.getConnection;

/**
 *
 * @author lucasfeijo
 */
public class BemDAOderby extends DAOderby implements BemDAO {

    private static BemDAOderby ref;
    
    private BemDAOderby() throws Exception {
        super();
    }
    
    public static BemDAOderby getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new BemDAOderby();
            } return ref;
        } catch (Exception ex) {
            throw new DAOException("Erro no BemDAOderby, "+ex.toString(), ex);
        }
    }

    @Override
    public List<Bem> buscar(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.BEM WHERE id_leilao=?");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            List<Bem> lista = new ArrayList<>();
            while(resultado.next()) {
                Bem p = new Bem(resultado.getString("NOME"),
                                resultado.getInt("QUANTIDADE"),
                                resultado.getInt("ID_LEILAO"));
                lista.add(p);
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public boolean inserir(Bem bem) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO APP.BEM (id_leilao, nome, quantidade) VALUES (?,?,?)");
            stmt.setInt(1, bem.getId_leilao());
            stmt.setString(2, bem.getNome());
            stmt.setInt(3, bem.getQuantidade());
            stmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (insercao), "+ex.toString(), ex);
        } catch (Exception ex) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex.toString(), ex);
        }
    }
    
}
