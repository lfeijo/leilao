/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.SQLException;

/**
 *
 * @author lucasfeijo
 */
public class DerbyHelper {
    
    /**
     *
     * @param e
     * @return se a exceção foi causada por criar uma tabela que já existe.
     */
    public static boolean tableAlreadyExists(SQLException e){
        return e.getSQLState().equals("X0Y32");
    }
    
}
