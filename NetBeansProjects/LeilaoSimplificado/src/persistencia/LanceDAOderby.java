/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import negocio.Lance;
import negocio.LanceDAO;
import static persistencia.DAOderby.getConnection;

/**
 *
 * @author lucasfeijo
 */
public class LanceDAOderby extends DAOderby implements LanceDAO {

    private static LanceDAOderby ref;
    
    private LanceDAOderby() throws DAOException, Exception {
        super();
    }
    
    public static LanceDAOderby getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new LanceDAOderby();
            } return ref;
        } catch (Exception ex) {
            throw new DAOException("Erro no LanceDAOderby, "+ex.toString(), ex);
        }
    }
    
    @Override
    public int inserir(Lance l) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO APP.LANCE (VALOR, TIME, ID_USER, ID_LEILAO) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setDouble(1, l.getValue());
            stmt.setLong(2, l.getTime());
            stmt.setInt(3, l.getId_user());
            stmt.setInt(4, l.getId_leilao());
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            int id;
            if(rs.next()) {
                id = rs.getInt(1);
                con.close();
                return id;
            }
        } catch (SQLException ex) {
            throw new DAOException("Falha no LanceDAOderby (insercao), "+ex.toString(), ex);
        } catch (Exception ex) {
            throw new DAOException("Falha no LanceDAOderby (conexao), "+ex.toString(), ex);
        }
        return 0;
    }

    @Override
    public List<Lance> buscar(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LANCE WHERE id_leilao=?");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            List<Lance> lista = new ArrayList<>();
            while(resultado.next()) {
                Lance p = new Lance(resultado.getDouble("valor"),
                                    resultado.getLong("time"),
                                    resultado.getInt("id_user"),
                                    resultado.getInt("id_leilao"));
                p.setId_lance(resultado.getInt("id_lance"));
                lista.add(p);
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public Lance buscarMenor(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LANCE WHERE id_leilao=? ORDER BY valor ASC");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Lance p = new Lance(resultado.getDouble("valor"),
                                    resultado.getLong("time"),
                                    resultado.getInt("id_user"),
                                    resultado.getInt("id_leilao"));
                p.setId_lance(resultado.getInt("id_lance"));
                return p;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public Lance buscarMaior(int id_leilao) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LANCE WHERE id_leilao=? ORDER BY valor DESC");
            stmt.setInt(1, id_leilao);
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Lance p = new Lance(resultado.getDouble("valor"),
                                    resultado.getLong("time"),
                                    resultado.getInt("id_user"),
                                    resultado.getInt("id_leilao"));
                p.setId_lance(resultado.getInt("id_lance"));
                return p;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }

    @Override
    public List<Lance> buscar(int idLeilao, int idUser) throws DAOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM APP.LANCE WHERE id_leilao=? AND id_user=?");
            stmt.setInt(1, idLeilao);
            stmt.setInt(2, idUser);
            ResultSet resultado = stmt.executeQuery();
            List<Lance> lista = new ArrayList<>();
            while(resultado.next()) {
                Lance p = new Lance(resultado.getDouble("valor"),
                                    resultado.getLong("time"),
                                    resultado.getInt("id_user"),
                                    resultado.getInt("id_leilao"));
                p.setId_lance(resultado.getInt("id_lance"));
                lista.add(p);
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOException("Falha no BemDAOderby (busca), "+ex.toString(), ex);
        } catch (Exception ex2) {
            throw new DAOException("Falha no BemDAOderby (conexao), "+ex2.toString(), ex2);
        }
    }
    
    
    
}
