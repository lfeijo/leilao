/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.SQLException;

/**
 *
 * @author lucasfeijo
 */
public class DAOException extends Exception {
    
    
    public DAOException(String msg){
        super(msg);
    }

    public DAOException(String msg, Exception e) {
        super(msg, e);
    }
}
