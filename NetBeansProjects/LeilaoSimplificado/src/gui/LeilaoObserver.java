/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import negocio.Leilao;

/**
 *
 * @author lucasfeijo
 */
interface LeilaoObserver {
    
    public void leilaoUpdated(Leilao l);
    
}
