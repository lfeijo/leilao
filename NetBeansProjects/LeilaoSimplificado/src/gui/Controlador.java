/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import gui.model.UserTableModel;
import gui.model.BensTableModel;
import gui.model.LanceTableModel;
import gui.model.LeilaoTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import negocio.Bem;
import negocio.FormException;
import negocio.Lance;
import negocio.Leilao;
import negocio.LeilaoFachada;
import negocio.LeilaoFinalizadoException;
import negocio.TipoLeilao;
import negocio.User;
import negocio.UserFachada;
import persistencia.DAOException;

/**
 *
 * @author lucasfeijo
 */
public class Controlador {
    
    /* Facades */
    private LeilaoFachada leilaoFachada;
    private UserFachada userFachada;
    
    /* Models */
    private LeilaoTableModel leilaoModel;
    private BensTableModel bensModel;
    private UserTableModel userModel;
    private LanceTableModel lanceModel;
    
    /* Statuses */
    //private Leilao currentLeilao;
    
    /* Views */
    private MainView mainView;
    private LoginView loginView;
    private UserCreateView userCreateView;
    private LeilaoCreateView leilaoCreateView;
    private LanceVencedorView lanceVencedorView;
    
    public Controlador() throws Exception {
        try {
            leilaoFachada = LeilaoFachada.getInstance();
            userFachada = UserFachada.getInstance();
            
            leilaoModel = new LeilaoTableModel(leilaoFachada.getLeilaoTodos());
            userModel = new UserTableModel(userFachada.buscarTodos());
            bensModel = new BensTableModel();
            lanceModel = new LanceTableModel();
            
            leilaoFachada.addLeilaoListener(leilaoModel);
            leilaoFachada.addBensListener(bensModel);
            userFachada.addUserListener(userModel);
            leilaoFachada.addLanceListener(lanceModel);
            
            mainView = new MainView(this);
            loginView = new LoginView(this);
            userCreateView = new UserCreateView(this);
            leilaoCreateView = new LeilaoCreateView(this);
            lanceVencedorView = new LanceVencedorView(this);
            
            mainView.setVisible(true);
            
        } catch (Exception ex) {
            throw new Exception("LeilaoController: "+ex.toString());
        }

    }
    
    /* LEILOES */
    
    public AbstractTableModel getLeilaoTableModel(){
        return leilaoModel;
    }
    
    public boolean insereLeilao(String inicio,
                                String fim,
                                String precoCondicao,
                                boolean fechado,
                                TipoLeilao tipo,
                                String categoria,
                                String descricao,
                                ArrayList<String> itens,
                                ArrayList<String> quantidades) throws FormException, DAOException, Exception {
        Leilao l = leilaoFachada.insereLeilao(inicio, fim, precoCondicao, fechado, tipo, categoria, descricao, itens, quantidades);
        return (l!=null);
    }
    
    public List<Leilao> buscarTodosLeiloes() throws FormException {
        return leilaoFachada.getLeilaoTodos();
    }

    public Leilao getLeilaoInRow(int selectedRow) {
        return leilaoModel.getLeilaoInRow(selectedRow);
    }

    public Leilao getCurrentLeilao() throws Exception {
        return LeilaoFachada.getInstance().getCurrentLeilao();
    }

    public void setCurrentLeilaoFromRow(int id) throws Exception {
        LeilaoFachada.getInstance().setCurrentLeilao(leilaoModel.getLeilaoInRow(id).getIdLeilao());
        mainView.leilaoUpdated(LeilaoFachada.getInstance().getCurrentLeilao());
        leilaoFachada.updateBensForCurrentLeilao(LeilaoFachada.getInstance().getCurrentLeilao());
        leilaoFachada.updateLancesForCurrentLeilao(LeilaoFachada.getInstance().getCurrentLeilao());
    }
    
    public boolean isLeilaoCorrendo(Leilao l) {
        return l.getFim()>System.currentTimeMillis()/1000;
    }
    
    public Leilao getLeilaoById(int id) throws FormException {
        return leilaoFachada.getLeilao(id);
    }

    /* LANCES */
    
    public int insereLance(String val) throws LeilaoFinalizadoException, FormException, Exception {
        return leilaoFachada.insereLance(LeilaoFachada.getInstance().getCurrentLeilao().getIdLeilao(), Double.parseDouble(val));
    }
    
    public AbstractTableModel getLancesTableModel(){
        return this.lanceModel;
    }
    
    public Lance getLanceVencedor(int id_leilao) throws DAOException, Exception {
        return LeilaoFachada.getInstance().getLanceVencedor(id_leilao);
    }
    
    /* BENS */
    
    public AbstractTableModel getBensTableModel(){
        return this.bensModel;
    }

    /* VIEWS */
    
    void viewLogin() {
        loginView.setVisible(true);
    }

    void viewLeilaoCreate() {
        leilaoCreateView.setVisible(true);
    }
    
    void viewUserCreate(){
        userCreateView.setVisible(true);
    }
    
    void viewLanceVencedor(){
        lanceVencedorView.setVisible(true);
        lanceVencedorView.initLance();
    }
    
    /* USERS */
    
    public AbstractTableModel getUserTableModel(){
        return userModel;
    }

    public boolean insereUser(String nome,
                              String id_gov,
                              String email) throws FormException, DAOException {
        User u = userFachada.insereUser(nome, id_gov, email);
        return (u!=null);
    }

    public List<User> buscarTodosUsers() throws FormException {
        return userFachada.buscarTodos();
    }

    public void setCurrentUserFromRow(int currentRow) {
        try {
            
            UserFachada.getInstance().setCurrentUser(userModel.getUserInRow(currentRow).getIdUser());
            mainView.userUpdated(UserFachada.getInstance().getCurrentUser().getNome());
                        
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public User getCurrentUser() {
        try {
            
            return UserFachada.getInstance().getCurrentUser();
            
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public User getUserById(int id) throws FormException {
        return userFachada.buscar(id);
    }
    
    /*  */
}
