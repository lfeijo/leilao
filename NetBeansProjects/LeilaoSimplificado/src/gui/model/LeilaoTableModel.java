/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import negocio.Leilao;
import negocio.LeilaoEvent;
import negocio.LeilaoListener;

/**
 *
 * @author lucasfeijo
 */
public class LeilaoTableModel extends AbstractTableModel implements LeilaoListener {

    private List<Leilao> leiloes;
    
    public LeilaoTableModel(){
        super();
    }
    
    public LeilaoTableModel(List<Leilao> leiloes) {
        this.leiloes = new ArrayList<Leilao>(leiloes);
    }

    @Override
    public int getRowCount() {
        return leiloes.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }
    
    @Override
    public String getColumnName(int i){
        switch (i){
            case 0: return "Inicio";
            case 1: return "Fim";
            case 2: return "Fechado";
        }
        return null;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return longToDate(leiloes.get(row).getInicio());
            case 1:
                return longToDate(leiloes.get(row).getFim());
            case 2:
                return leiloes.get(row).isFechado()?"Sim":"Não";
        }
        return null;
    }

    @Override
    public void elementoAdicionado(LeilaoEvent event) {
        add(event.getLeilao());
    }

    private void add(Leilao leilao) {
        leiloes.add(leilao);
        fireTableRowsInserted(leiloes.size()-1, leiloes.size()-1);
    }
    
    public Leilao getLeilaoInRow(int row){
        return leiloes.get(row);
    }

    private String longToDate(long unix) {
        Date time=new java.util.Date((long)unix*1000);
        return time.toString();
    }
    
}
