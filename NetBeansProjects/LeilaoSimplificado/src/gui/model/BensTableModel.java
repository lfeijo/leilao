/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import negocio.Bem;
import negocio.BensEvent;
import negocio.BensListener;

/**
 *
 * @author lucasfeijo
 */
public class BensTableModel extends AbstractTableModel implements BensListener {
    
    private List<Bem> bens;
    
    public BensTableModel(){
        super();
        bens = new ArrayList();
    }
    
    public BensTableModel(List<Bem> bens){
        this.bens = new ArrayList<>(bens);
    }

    @Override
    public int getRowCount() {
        return bens.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return bens.get(rowIndex).getNome();
            case 1:
                return bens.get(rowIndex).getQuantidade()+"";
        }
        return null;
    }
    
    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Descrição";
            case 1:
                return "Quantidade";
        }
        return null;
    }

    @Override
    public void bensAtualizados(BensEvent evt) {
        this.bens = evt.getBens();
        fireTableDataChanged();
    }
    
}
