package gui.model;

import java.util.ArrayList;
import negocio.UserListener;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import negocio.User;
import negocio.UserEvent;

public class UserTableModel extends AbstractTableModel implements UserListener {

    private List<User> users;

    public UserTableModel(){
        super();
    }
    
    public UserTableModel(List<User> users) {
        this.users = new ArrayList<User>(users);
    }

    public void add(User user) {
        users.add(user); // direto da fachada, esses valores não fora pegos do DB
        System.out.println(users.toString());
        fireTableRowsInserted(users.size()-1, users.size()-1);
    }

    /**
     * @param event
     * @see negocio.UserListener#elementoAdicionado(negocio.UserEvent)
     */
    @Override
    public void elementoAdicionado(UserEvent event) {
        add(event.getUser());
    }
    
    @Override
    public int getRowCount(){
        return users.size();
    }
    
    @Override
    public int getColumnCount(){
        return 3;
    }
    
    @Override
    public String getColumnName(int i){
        switch (i){
            case 0: return "Nome";
            case 1: return "CPF/CNPJ";
            case 2: return "Email";
        }
        return null;
    }
    
    @Override
    public Object getValueAt(int row, int column){
        switch (column) {
            case 0: // nome
                return users.get(row).getNome();
            case 1: // id_gov
                return users.get(row).getIdGov();
            case 2: // email
                return users.get(row).getEmail();
        }
        return null;
    }
    
    public User getUserInRow(int id) {
        return users.get(id);
    }

}
