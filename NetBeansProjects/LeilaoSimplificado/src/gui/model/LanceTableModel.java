/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import negocio.Lance;
import negocio.LanceEvent;
import negocio.LanceListener;

/**
 *
 * @author lucasfeijo
 */
public class LanceTableModel extends AbstractTableModel implements LanceListener {
    
    private List<Lance> lances;

    public LanceTableModel(){
        super();
        lances = new ArrayList<>();
    }
    
    public LanceTableModel(List<Lance> lances){
        this.lances = new ArrayList<>(lances);
    }
    
    @Override
    public int getRowCount() {
        return lances.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return lances.get(rowIndex).getValue()+"";
            case 1:
                return lances.get(rowIndex).getId_user()+"";
            case 2:
                return longToDate(lances.get(rowIndex).getTime())+"";
        }
        return null;
    }
    
    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "Valor";
            case 1:
                return "Usuário";
            case 2:
                return "Hora";
        }
        return null;
    }

    @Override
    public void elementoAdicionado(LanceEvent evt) {
        lances.add(evt.getLance().get(0));
        fireTableRowsInserted(lances.size(), lances.size());
    }
    
    private String longToDate(long unix) {
        Date time=new java.util.Date((long)unix*1000);
        return time.toString();
    }

    @Override
    public void resetAndAdd(LanceEvent evt) {
        lances = evt.getLance();
        fireTableDataChanged();
    }
    
}
