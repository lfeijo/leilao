package negocio;

import java.util.EventObject;

public class UserEvent extends EventObject {

    private User user;
        
    public UserEvent(Object source, User u) {
        super(source);
        user = u;
    }

    public User getUser() {
        return user;
    }

}
