/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.EventObject;
import java.util.List;

/**
 *
 * @author lucasfeijo
 */
public class BensEvent extends EventObject {
    
    private List<Bem> bens;
    
    public BensEvent(Object source, List<Bem> bens) {
        super(source);
        this.bens = bens;
    }
    
    public List<Bem> getBens() {
        return bens;
    }
}
