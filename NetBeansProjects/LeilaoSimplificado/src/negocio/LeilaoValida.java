/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author lucasfeijo
 */
public class LeilaoValida {
    
    public static boolean validaInicioFim(long inicio, long fim) {
        return inicio < fim;
    }
    
    public static boolean validaLance(Leilao leilao, Lance lance) {
        switch (leilao.getTipo()) {
            case LEILAO_DEMANDA:
                return leilao.getPrecoCondicao()<=lance.getValue();
            case LEILAO_OFERTA:
                return leilao.getPrecoCondicao()>=lance.getValue();
        }
        return false;
    }
    
}
