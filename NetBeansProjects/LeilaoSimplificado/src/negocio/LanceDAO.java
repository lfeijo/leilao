/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.List;
import persistencia.DAOException;

/**
 *
 * @author lucasfeijo
 */
public interface LanceDAO {
    
    public int inserir(Lance lance) throws DAOException;
    
    public List<Lance> buscar(int id_leilao) throws DAOException;

    public Lance buscarMenor(int id_leilao) throws DAOException;

    public Lance buscarMaior(int id_leilao) throws DAOException;

    public List<Lance> buscar(int idLeilao, int idUser) throws DAOException;
    
}
