/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.List;

/**
 *
 * @author lucasfeijo
 */
public class Lote {
    private String categoria;
    private String descricao;
    private List<Bem> bens;
    private int id_leilao;
    
    public Lote(){
        
    }

    public Lote(String categoria, String descricao, int id_leilao) {
        this.categoria = categoria;
        this.descricao = descricao;
        this.id_leilao = id_leilao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Bem> getBens() {
        return bens;
    }

    public void setBens(List<Bem> bens) {
        this.bens = bens;
    }
    
    public void setIdLeilao(int id){
        this.id_leilao = id;
    }
    
    public int getIdLeilao(){
        return id_leilao;
    }
    
}
