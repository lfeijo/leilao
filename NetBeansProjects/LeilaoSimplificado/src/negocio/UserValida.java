package negocio;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class UserValida {

    public static boolean validaNome(String nome){
        return nome.trim().split(" ").length>=2; // 2 palavras ou mais
    }

    public static boolean validaID(String id_gov) {
        return Pattern.compile("([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})").matcher(id_gov).matches();
    }
    
    public static boolean validaEmail(String email){
        
        return Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches();
    }

}