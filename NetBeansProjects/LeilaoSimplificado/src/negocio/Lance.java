/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author lucasfeijo
 */
public class Lance {
    
    private int id_lance;
    
    private double value;
    
    private long time;
    
    private int id_user;
    
    private int id_leilao;

    public Lance(double value, long time, int id_user, int id_leilao) {
        this.value = value;
        this.time = time;
        this.id_user = id_user;
        this.id_leilao = id_leilao;
    }

    public int getId_lance() {
        return id_lance;
    }

    public void setId_lance(int id_lance) {
        this.id_lance = id_lance;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_leilao() {
        return id_leilao;
    }

    public void setId_leilao(int id_leilao) {
        this.id_leilao = id_leilao;
    }


      
}
