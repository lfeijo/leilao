/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author lucasfeijo
 */
public class FormException extends Exception {
    
    public FormException(){
        
    }

    public FormException(String msg) {
        super(msg);
    }

    public FormException(String msg, Throwable e) {
        super(msg, e);
    }
    
}
