/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author lucasfeijo
 */
public class Leilao {
    
    private int id_leilao;
    private long inicio;
    private long fim;
    private double precoCondicao;
    private boolean fechado;
    private TipoLeilao tipo;
    private Lote lote;
    private int id_user;

    public Leilao(int id_leilao,
                  long inicio,
                  long fim,
                  double precoCondicao,
                  boolean fechado,
                  TipoLeilao tipo,
                  int id_user) {
        this.id_leilao = id_leilao;
        this.inicio = inicio;
        this.fim = fim;
        this.precoCondicao = precoCondicao;
        this.fechado = fechado;
        this.tipo = tipo;
        this.id_user = id_user;
    }
    
    public int getIdLeilao(){
        return this.id_leilao;
    }
    
    public void setIdLeilao(int id){
        this.id_leilao = id;
    }

    public long getInicio() {
        return inicio;
    }

    public void setInicio(long inicio) {
        this.inicio = inicio;
    }

    public long getFim() {
        return fim;
    }

    public void setFim(long fim) {
        this.fim = fim;
    }

    public double getPrecoCondicao() {
        return precoCondicao;
    }

    public void setPrecoCondicao(double precoCondicao) {
        this.precoCondicao = precoCondicao;
    }

    public boolean isFechado() {
        return fechado;
    }

    public void setFechado(boolean fechado) {
        this.fechado = fechado;
    }

    public TipoLeilao getTipo() {
        return tipo;
    }

    public void setTipo(TipoLeilao tipo) {
        this.tipo = tipo;
    }

    public Lote getLote() {
        return lote;
    }

    public void setLote(Lote lote) {
        this.lote = lote;
    }    
    
    public int getTipoAsInt(){
        switch ((TipoLeilao)this.tipo) {
            case LEILAO_OFERTA:
                return 0;
            case LEILAO_DEMANDA:
                return 1;
        }
        return -1;
    }
    
    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
    
}
