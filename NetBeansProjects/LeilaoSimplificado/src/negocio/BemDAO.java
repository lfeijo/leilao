/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.List;
import persistencia.DAOException;

/**
 *
 * @author lucasfeijo
 */
public interface BemDAO {
    
    public List<Bem> buscar(int id_leilao) throws DAOException;
    
    public boolean inserir(Bem bem) throws DAOException;
    
}
