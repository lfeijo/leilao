/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.EventObject;
import java.util.List;

/**
 *
 * @author lucasfeijo
 */
public class LanceEvent extends EventObject {
    
    private List<Lance> lance;
    
    public LanceEvent(Object source, List<Lance> lance) {
        super(source);
        this.lance = lance;
    }
    
    public List<Lance> getLance(){
        return this.lance;
    }
    
}
