/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author lucasfeijo
 */
public class Bem {
    private String nome;
    private int quantidade;
    private int id_leilao;

    public Bem(String nome, int quantidade, int id_leilao){
        this.nome=nome;
        this.quantidade=quantidade;
        this.id_leilao=id_leilao;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getId_leilao() {
        return id_leilao;
    }

    public void setId_leilao(int id_leilao) {
        this.id_leilao = id_leilao;
    }
    
    
}
