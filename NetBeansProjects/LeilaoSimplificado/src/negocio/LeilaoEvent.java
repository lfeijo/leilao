/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.EventObject;

/**
 *
 * @author lucasfeijo
 */
public class LeilaoEvent extends EventObject {

    private Leilao leilao;
        
    public LeilaoEvent(Object source, Leilao l) {
        super(source);
        leilao = l;
    }

    public Leilao getLeilao() {
        return leilao;
    }

}
