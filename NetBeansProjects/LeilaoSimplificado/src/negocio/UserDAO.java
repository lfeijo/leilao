package negocio;

import java.util.List;
import persistencia.DAOException;

public interface UserDAO {

    public abstract int inserir(User user) throws DAOException;

    public abstract List<User> buscar() throws DAOException;

    public User buscar(int id_user) throws DAOException;

}
