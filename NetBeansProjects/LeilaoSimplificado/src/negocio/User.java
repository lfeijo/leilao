package negocio;

public class User {

    private int id_user;

    private String nome;

    private String id_gov;

    private String email;

    public int getIdUser(){ return id_user; }
    public String getNome(){ return nome; }
    public String getIdGov(){ return id_gov; }
    public String getEmail(){ return email; }
    
    public User(String nome, String id_gov, String email) {
        this.nome = nome;
        this.id_gov = id_gov;
        this.email = email;
    }
    
    public User(int id_user, String nome, String id_gov, String email) {
        this.id_user = id_user;
        this.nome = nome;
        this.id_gov = id_gov;
        this.email = email;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
    
    public String toString(){
        return id_user+": "+nome;
    }

    
}
