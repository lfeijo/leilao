/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import persistencia.BemDAOderby;
import persistencia.DAOException;
import persistencia.LanceDAOderby;
import persistencia.LeilaoDAOderby;
import persistencia.LoteDAOderby;
import persistencia.UserDAOderby;
import negocio.Lance;
import negocio.Bem;

/**
 *
 * @author lucasfeijo
 */
public class LeilaoFachada {
    
    private static LeilaoFachada ref;
    
    private LeilaoDAO leilaoDAO;
    private LoteDAO loteDAO;
    private BemDAO bemDAO;
    private LanceDAO lanceDAO;
    private UserDAO userDAO;
    
    private List<LeilaoListener> leilaoListeners;
    private List<BensListener> bensListeners;
    private List<LanceListener> lanceListeners;
    
    private Leilao currentLeilao;
    
    private LeilaoFachada() throws Exception {
        try {
            leilaoDAO = LeilaoDAOderby.getInstance();
            loteDAO = LoteDAOderby.getInstance();
            bemDAO = BemDAOderby.getInstance();
            lanceDAO = LanceDAOderby.getInstance();
            userDAO = UserDAOderby.getInstance();
        } catch (Exception ex) {
            throw new FormException("LeilaoFachada: "+ex.toString(), ex);
        }
        
        leilaoListeners = new ArrayList<>();
        bensListeners = new ArrayList<>();
        lanceListeners = new ArrayList<>();
    }
    
    public static LeilaoFachada getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new LeilaoFachada();
            } return ref;
        } catch (Exception ex) {
            throw new Exception("LeilaoFachada: "+ex.toString(), ex);
        }

    }
  
    public void addLeilaoListener(LeilaoListener l) {
        if (!leilaoListeners.contains(l)) {
            leilaoListeners.add(l);
        }
    }
    
    public void removeLeilaoListener(LeilaoListener l) {
        leilaoListeners.remove(l);
    }

    public Leilao getLeilao(int id_leilao) throws FormException {
        try {
            Leilao leilao = leilaoDAO.buscar(id_leilao);
            leilao.setLote(loteDAO.buscar(leilao.getIdLeilao()));
            leilao.getLote().setBens(bemDAO.buscar(leilao.getIdLeilao()));
            return leilao;
            
        } catch (DAOException ex) {
            throw new FormException("Falha ao buscar leiloes! "+ex.toString(), ex);
        }
    }
    
    public List<Leilao> getLeilaoTodos() throws FormException {
        try {
            List<Leilao> leiloes = leilaoDAO.buscar();
            
            for(Leilao l : leiloes){
                l.setLote(loteDAO.buscar(l.getIdLeilao()));
                l.getLote().setBens(bemDAO.buscar(l.getIdLeilao()));
            }
            return leiloes;
            
        } catch (Exception ex) {
            throw new FormException("Falha ao buscar leiloes! "+ex.toString(), ex);
        }
    }

    public Leilao insereLeilao(String inicio,
                               String fim,
                               String precoCondicao,
                               boolean fechado,
                               TipoLeilao tipo,
                               String categoria,
                               String descricao,
                               ArrayList<String> itens,
                               ArrayList<String> quantidades) throws FormException, DAOException, Exception {
        try {
            if     (inicio.isEmpty() ||
                    fim.isEmpty() ||
                    precoCondicao.isEmpty() ||
                    categoria.isEmpty() ||
                    descricao.isEmpty() ||
                    itens.isEmpty() ||
                    quantidades.isEmpty()) { 
                throw new FormException("Erro: Preencha todos os campos.");
            }
        } catch (NullPointerException ex) {
            throw new FormException("Erro: Preencha todos os campos.");
        }
        for (int i = 0; i < itens.size(); i++) {
            if (itens.get(i).isEmpty() || quantidades.get(i).isEmpty()) {
                throw new FormException("Erro: Preencha quantidades para todos os itens.");
            }
        }
        
        long dinicio = getLongFromString(inicio);
        long dfim = getLongFromString(fim);
        double dpreco = Double.parseDouble(precoCondicao);
        
        if (!LeilaoValida.validaInicioFim(dinicio, dfim)) {
            throw new FormException("Erro: Data de fim menor que de inicio.");
        }
        Leilao l = new Leilao(0, dinicio, dfim, dpreco, fechado, tipo, UserFachada.getInstance().getCurrentUser().getIdUser());        
        l.setIdLeilao(leilaoDAO.inserir(l));
        Lote lote = new Lote(categoria, descricao, l.getIdLeilao());
        ArrayList<Bem> bens = new ArrayList<>();
        for (int i = 0; i < itens.size(); i++) {
            bens.add(new Bem(itens.get(i), Integer.parseInt(quantidades.get(i)), l.getIdLeilao()));
        }
        loteDAO.inserir(lote);
        for (int i = 0; i < bens.size(); i++) {
            bemDAO.inserir(bens.get(i));
        }
        l.setLote(lote);
        l.getLote().setBens(bens);
        notificaLeilaoListeners(l);
        return l;
        // verifica infos recebidas, passa pro dao e notifica listeners.
    }
    
    private long getLongFromString(String val) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss z yy");
        return dateFormat.parse(val).getTime()/1000;
    }
    
    protected void notificaLeilaoListeners(Leilao leilao) {
        LeilaoEvent evt = new LeilaoEvent(this, leilao);
        for(LeilaoListener l : leilaoListeners) {
            l.elementoAdicionado(evt);
        }
    }
    
    public List<Bem> insereBens(List<Bem> bens) throws FormException, DAOException {
        for (Bem b : bens) {
            bemDAO.inserir(b);
        }
        notificaBensListeners(bens);
        return bens;
    }

    public void addBensListener(BensListener bensModel) {
        if (!bensListeners.contains(bensModel)) {
            bensListeners.add(bensModel);
        }
    }
    
    protected void notificaBensListeners(List<Bem> bens) {
        BensEvent evt = new BensEvent(this, bens);
        for (BensListener l : bensListeners) {
            l.bensAtualizados(evt);
        }
    }
    
    protected void notificaResetLanceListeners(List<Lance> lance) {
        LanceEvent evt = new LanceEvent(this, lance);
        for (LanceListener l : lanceListeners) {
            l.resetAndAdd(evt);
        }
    }
    
    protected void notificaLanceListeners(Lance lance) {
        LanceEvent evt = new LanceEvent(this, Arrays.asList(lance));
        for (LanceListener l : lanceListeners) {
            l.elementoAdicionado(evt);
        }
    }

    public int insereLance(int id_leilao, double val) throws LeilaoFinalizadoException, FormException, Exception {
        if (this.getCurrentLeilao().getFim()<(System.currentTimeMillis()/1000)) {
            throw new LeilaoFinalizadoException();
        }
        Lance l = new Lance (val,
                            (System.currentTimeMillis()/1000),
                            UserFachada.getInstance().getCurrentUser().getIdUser(),
                            id_leilao);
        if (LeilaoValida.validaLance(this.getLeilao(id_leilao), l)) {
            l.setId_lance(lanceDAO.inserir(l));
            notificaLanceListeners(l);
            return l.getId_lance();
        } else {
            throw new FormException("Valor de lance inválido, verificar valores máximos e mínimos.");
        }
    }
    
    public void updateBensForCurrentLeilao(Leilao currentLeilao) {
        notificaBensListeners(currentLeilao.getLote().getBens());
    }

    public void updateLancesForCurrentLeilao(Leilao currentLeilao) throws Exception {
        try {
            if (!currentLeilao.isFechado()) {
                notificaResetLanceListeners(lanceDAO.buscar(currentLeilao.getIdLeilao()));
            } else {
                notificaResetLanceListeners(lanceDAO.buscar(currentLeilao.getIdLeilao(), UserFachada.getInstance().getCurrentUser().getIdUser()));
            }
        } catch (DAOException ex) {
            // idk
        }
        
    }
    
    public void updateNewLanceForLeilao(Lance lance) {
        notificaLanceListeners(lance);
    }
    
    public void addLanceListener(LanceListener l) {
        if (!lanceListeners.contains(l)) {
            lanceListeners.add(l);
        }
    }
    
    public void setCurrentLeilao(int id_leilao) throws DAOException, FormException {
        if (currentLeilao==null) {
            this.currentLeilao = this.getLeilao(id_leilao);
        } else if (currentLeilao.getIdLeilao()!=id_leilao) {
            this.currentLeilao = this.getLeilao(id_leilao);
        } else {
            // setando mesmo leilao... ignorar
        }
        
    }
    
    public Leilao getCurrentLeilao() {
        return this.currentLeilao;
    }

    public Lance getLanceVencedor(int id_leilao) throws DAOException {
        if (currentLeilao.getTipo()==TipoLeilao.LEILAO_DEMANDA) {
            return lanceDAO.buscarMaior(id_leilao);
        } else if (currentLeilao.getTipo()==TipoLeilao.LEILAO_OFERTA) {
            return lanceDAO.buscarMenor(id_leilao);
        }
        return null;
    }

}
