package negocio;

import gui.MainView;
import java.util.ArrayList;
import java.util.List;
import persistencia.DAOException;
import persistencia.UserDAOderby;

public class UserFachada {
    
    private static UserFachada ref;

    private UserDAO dao;

    private List<UserListener> listeners;
    
    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(int currentUserId) throws FormException {
        try {
            this.currentUser = dao.buscar(currentUserId);
        } catch (DAOException ex) {
            throw new FormException("UserFachada: "+ex.toString(), ex);
        }
    }
    
    private UserFachada() throws Exception {
        try {
            dao = UserDAOderby.getInstance();
        } catch (Exception ex) {
            throw new FormException(ex.toString(), ex);
        }
        
        listeners = new ArrayList<>();
    }
    
    public static UserFachada getInstance() throws Exception {
        try {
            if (ref == null) {
                ref = new UserFachada();
            } return ref;
        } catch (Exception ex) {
            throw new Exception("UserFachada: "+ex.toString(), ex);
        }
    }
    
    public void addUserListener(UserListener l) {
        if(!listeners.contains(l)) {
            listeners.add(l);
        }
    }

    public void removeCadastroListener(UserListener l) {
        listeners.remove(l);
    }

    public User insereUser(String nome,
                           String id_gov,
                           String email) throws FormException, DAOException {
        if (!UserValida.validaNome(nome)) {
            throw new FormException("Nome inválido, deve ser nome completo.");
        }
        if (!UserValida.validaID(id_gov)) {
            throw new FormException("CPF ou CNPJ inválido, deve ser: \n\tXXX.XXX.XXX-XX (CPF)\n\tXX.XXX.XXX/XXXX-XX (CNPJ)");
        }
        if (!UserValida.validaEmail(email)) {
            throw new FormException("Email inválido, deve ser x@y.z");
        }
        
        User u = new User(nome, id_gov, email);
        
        try {
            
            u.setId_user(dao.inserir(u));
            notificaListeners(u);
            return u;
            
        } catch (DAOException ex) {
            throw new FormException(ex.toString(), ex);
        }
        
    }

    protected void notificaListeners(User user) {
        UserEvent evt = new UserEvent(this, user);
        for(UserListener l : listeners) {
            l.elementoAdicionado(evt);
        }
    }

    public List<User> buscarTodos() throws FormException{
        try {
            return dao.buscar();
        } catch (DAOException e) {
            throw new FormException("Falha ao buscar pessoas! "+e.toString(), e);
        }
    }

    public User buscar(int id) throws FormException {
        try {
            return dao.buscar(id);
        } catch (DAOException e) {
            throw new FormException("Falha ao buscar pessoas! "+e.toString(), e);
        }
    }

}
